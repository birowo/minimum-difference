package main

import (
	"fmt"
)

func getMinimumDifferent(a, b []string) (rslt []int) {
	aLen := len(a)
	rslt = make([]int, 0, aLen)
	aMap := make([]map[rune]int, aLen)
	for i, str := range a {
		aMap[i] = make(map[rune]int, len(str))
		for _, chr := range str {
			aMap[i][chr]++
		}
	}
	for i, str := range b {
		diff := 0
		if len(a[i]) == len(b[i]) {
			aMapI := aMap[i]
			for _, chr := range str {
				_, ok := aMapI[chr]
				if ok {
					aMapI[chr]--
					if aMapI[chr] < 0 {
						diff++
					}
				} else {
					diff++
				}
			}
			rslt = append(rslt, diff)
		} else {
			rslt = append(rslt, -1)
		}
	}

	return
}
func main() {
	a := []string{"a", "jk", "abb", "mn", "abc"}
	b := []string{"bb", "kj", "bbb", "op", "def"}
	rslt := getMinimumDifferent(a, b)
	fmt.Println(rslt)
}
